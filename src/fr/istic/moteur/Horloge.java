package fr.istic.moteur;

import fr.istic.command.Command;

public interface Horloge {
    /**
     * Activation de la commande cmd périodiquement par periodeEnMiliSecondes
     * @param cmd la commande a active
     * @param periodeEnMiliSecondes periodeEnMiliSecondes
     */
	void activerPeriodiquement(Command cmd, float  periodeEnMiliSecondes);

/**
 *  desactivation de l'horloge 
 */
	void desactiver();
	
	
}
