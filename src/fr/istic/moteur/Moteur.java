package fr.istic.moteur;

import fr.istic.command.Command;


public interface Moteur {

	/** renvoie la propriete qui définit  nombre de temps par mesure
	 * 
	 * @return la propriete qui définit  nombre de temps par mesure.
	 */
	public int getMesure();
    /**
     *  Assignation de la propriete  de mesure 
     * @param mesure la propriete  de mesure 
     */
	public void setMesure(int mesure);

	/**
	 * Renvoie la propriete qui definit le nombre de batement par minute 
	 * @return la propriete qui definit le nombre de batement par minute
	 */
	public float gettempo();
/**
 *  Incremente la propriete qui definit  nombre de temps par mesure
 */
	public void incMesure();
/**
 * Décrémente la propriete qui definit  nombre de temps par mesure
 */
	public void decMesure();
/**
 * Assignation de la commande flashTic 
 * @param ctic la commande flashTic 
 */
	void setCmdTic(Command ctic);
/**
 * assignation de la commande UpdateMesure et UpdateTempo au Moteur
 * @param c1 la commande UpdateMesure
 * @param c2 la commande UpdateTempo
 */
	public void setCmdUpdate(Command c1, Command c2);
/**
 * Renvoie l'horloge 
 * @return l'horloge
 */
	public Horloge getHorloge();

	/**
	 * Assignation de  la propriete qui definit le nombre de batement par minute
	 * @param a la propriete qui definit le nombre de batement par minute
	 */
	public void settempos(int a);

	/**
	 * Renvoie la propriete qui definit l'etat de metronome 
	 * 
	 * @return true si EnMarche 
	 *         false si non
	 */
	public boolean getEtat();

	/**
	 * Assignation de la propriete qui definit l'etat de metronome 
	 * @param etat la propriete qui definit l'état de metronome
	 */
	public void setEtat(boolean etat);

	

	/**
	 * Creer une horloge 
	 */
	void createHorloge();
	


	

}
