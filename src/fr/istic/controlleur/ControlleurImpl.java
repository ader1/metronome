package fr.istic.controlleur;

import fr.istic.moteur.Moteur;
import fr.istic.vue.Vue;

public class ControlleurImpl implements Controlleur {

	Moteur moteur;
	Vue vue;
	int nbTempo;

	public ControlleurImpl(Moteur moteur) {
		super();
		this.moteur = moteur;
		nbTempo = 0;
	}

	public void setVue(Vue v) {
		this.vue = v;
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		moteur.setEtat(true);
		moteur.createHorloge();

	}

	@Override
	public void inc() {
		moteur.incMesure();

	}

	@Override
	public void dec() {
		// TODO Auto-generated method stub

		moteur.decMesure();
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		moteur.setEtat(false);
		moteur.getHorloge().desactiver();
		if (vue != null) {
			nbTempo = 0;
			vue.eteindled();
		}
	}

	@Override
	public void cursseur() {
		// TODO Auto-generated method stub
		if (vue != null) {
			System.out.println("cursseur moteur");
			int valuetempo = (int) vue.getvaleurslider();
			moteur.settempos(valuetempo);
		}
	}

	@Override
	public void updateTempo() {
		// TODO Auto-generated method stub
		if (vue != null) vue.setlabel((int) moteur.gettempo());

	}

	@Override
	public void tic() {
		// TODO Auto-generated method stub

		nbTempo++;
		System.out.println("nbtompo : " + nbTempo);
		if (nbTempo >= moteur.getMesure()) {
			System.out.println(moteur.getMesure());
			nbTempo = 0;
			if (vue != null)
				vue.allumerled2();
		}
		if (vue != null)
			vue.allumerled1();
		// vue.eteindled1();

	}

	@Override
	public void updateMesure() {
		// TODO Auto-generated method stub
		if (vue != null)
			vue.updateMeasure(moteur.getMesure());
	}

}
