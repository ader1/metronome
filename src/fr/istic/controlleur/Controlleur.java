package fr.istic.controlleur;

public interface Controlleur {
	/**
	 *  Démarrer le metronome(assigner etat=true)  
	 */
void start();
/**
 * Augmente le nombre de temps dans une mesure d'une unite
 */
void inc();
/**
 * dimininue le nombre de temps dans une mesure d'une unite
 */
void dec();
/**
 * arrete le moteur (assigner etat=false)
 */
void stop();
/**
 * Mettre a jour l'etat de tempo dans le modele 
 */
void cursseur();
/**
 * Mettre a jour l'afficheur du tempo
 */
void updateTempo();
/**
 * Emettre un eclair et un clic sonore pour led1 et led 2
 */

void tic ();
/**
 *  Mettre a jour la mesure 
 */
void updateMesure();

}
