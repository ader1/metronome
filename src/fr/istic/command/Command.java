package fr.istic.command;

public interface Command {
/**
 * execute la command
 */
	void execute();
}
